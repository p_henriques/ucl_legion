function [] = gcmultiextralign_legion(datadir)

set(0,'DefaultFigureVisible','off')

movthr_um = 5; % motion artifact threshold in microns
sigma_um = 0.8; % width of smoothing gaussian (um) for gcfilter

%% BASIC POST-PROCESSING

gcextr_5(datadir);

gcalign_4(datadir,movthr_um,4);

load(fullfile(datadir,'gm'),'gm');

thissigma = sigma_um./gm.xpx;

gcfilter('gaussian', thissigma, datadir);

gcv(datadir); % visstim triggered dF/F

gcga(datadir); % pixelwise activity

%% BEHAVIOR

gcextrbehavior_10(datadir);

gcextracttails_4(datadir);

gcJturn_v103(datadir, true); % must have run gcextrbehavior and gcextracttails first

gcdFF_conv_v4(datadir);

%% BOUT CATEGORISATION AND BOUT-TYPE ANALYSES

gcbft(datadir);

gcbft_fmap(datadir);

%% ANAT ROIS

gcautoseganat_nuc(datadir);

% gcautoseganat_v202(datadir) % automatic watershed-based ROI detection

gcROIAllResponses_anat(datadir,1) % computes ROI-ts for anat ROIs

gcVCNV(datadir)  % classifies ROIs as visually responsive or not

end