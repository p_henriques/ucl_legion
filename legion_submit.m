function [] = legion_submit(fun2run,fileslist,inargs,noutargs,nthreads,mins,memGB,userid)
% Submits a job to UCL's Legion cluster
%
% Requires user to have already configured a MATLAB 2016b Legion cluster
% profile (https://wiki.rc.ucl.ac.uk/wiki/Full_Matlab_on_Legion);
%
% ======== Input arguments ==========
% fun2run (char vector) - Function to run (eg., 'my_function.m')
% fileslist (cell array) - Any aditional files or folders to transfer
% inargs (cell array) - Input arguments for function (eg., {rand(10),'input2'})
% noutargs (double) - Number of output arguments
% nthreads (double) - Number of threads to request (eg, 16 - see Legion doc)
% mins (double) - Amount of time to request for the job to run (in minutes)
% memGB (double) - Amount of memory to request in GB/thread
% userid (char) - Your UCL userID (eg., ucbtphd)
% ===================================
%
% Pedro Henriques, April 2018

% Create function handle
fh = str2func(fun2run);

% Get function dependencies
fundeps = matlab.codetools.requiredFilesAndProducts(fun2run);

% Concatenate any additional files required
fileslist = cat(2,fileslist,fundeps);

%% Setup cluster parameters

% Requested amount of time in minutes
hm = [fix(mins/60) rem(mins,60)];

% Set variables
setenv ('SGE_CONTEXT', 'exclusive');
setenv ('SGE_OPT', sprintf('h_rt=%d:%d:0,mem=%dG',hm(1),hm(2),memGB)); 
setenv ('MATLAB_PREFDIR', sprintf('/home/%s/Scratch/matlab_prefs/$JOB_ID/prefs',userid))

% Create cluster job
c = parcluster('legion_R2016b');
myJob = createCommunicatingJob(c, 'Type', 'Pool');
myJob.AttachedFiles = fileslist;
myJob.NumWorkersRange = [nthreads, nthreads];
task = createTask(myJob, fh, noutargs, inargs);

% Submit job
submit(myJob);

end